package Utils;

public class Constants {

    public static final String DRIVER = "webdriver.chrome.driver";
    public static final String DRIVER_LOCAL_PATH = "C:\\Users\\ivana\\Desktop\\QA\\Chromedriver\\chromedriver_win32 (3)\\chromedriver.exe";
    public static final String GOOGLE_URL = "https://www.google.com";
    public static final String BING_URL = "https://www.bing.com";

    public static final String SEARCH_MESSAGE = "Telerik Academy Alpha";

    public static final String EXPECTED_RESULT = "IT Career Start in 6 Months - Telerik Academy Alpha";
    public static final String ERROR_MESSAGE = "Search result doesn't match.";

}
