import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;
import static Utils.Constants.*;


public class GoogleChromeTest {
    //navigate to Google Chrome
    @Test
    public void searchWithGoogleChrome() {
        System.setProperty(DRIVER,
                DRIVER_LOCAL_PATH);

        //navigate to Google
        WebDriver webDriver = new ChromeDriver();
        webDriver.navigate().to(GOOGLE_URL);
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //accepting cookies
        WebElement button = webDriver.findElement(By.xpath("/html/body/div[2]/div[2]/div[3]/span/div/div/div/div[3]/div[1]/button[2]"));
        button.click();

        //enter text in search box and click on it
        WebElement item = webDriver.findElement(By.name("q"));
        item.sendKeys(SEARCH_MESSAGE);
        item.submit();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


        //validate title of first result
        WebElement searchResult = webDriver.findElement(By.xpath("/html/body/div[7]/div/div[10]/div[1]/div[2]/div[2]/div/div/div[1]/div/div[1]/div/a/h3"));

        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


        Assert.assertEquals(searchResult.getText(), EXPECTED_RESULT, ERROR_MESSAGE);


        webDriver.quit();

    }
}
