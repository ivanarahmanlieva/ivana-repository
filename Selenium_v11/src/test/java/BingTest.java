import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;
import static Utils.Constants.*;


public class BingTest {


    @Test
    public void searchWithBing() {
        System.setProperty(DRIVER,
                DRIVER_LOCAL_PATH);

        //navigate to Bing
        WebDriver webDriver = new ChromeDriver();
        webDriver.navigate().to(BING_URL);
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //enter text in search box and click on it
        WebElement item = webDriver.findElement(By.name("q"));
        item.sendKeys(SEARCH_MESSAGE);
        item.submit();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //validate title of first result
        WebElement searchResult = webDriver.findElement(By.xpath("//*[@id='b_results']/li[1]/div[1]/h2"));

        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        Assert.assertEquals(searchResult.getText(), EXPECTED_RESULT,
                ERROR_MESSAGE);

        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.quit();

    }

}
