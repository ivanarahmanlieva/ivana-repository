import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.concurrent.TimeUnit;

public class Bing {


    @Test
    public void searchWithBing() {
        System.setProperty("webdriver.chrome.driver",
                "C:\\Users\\ivana\\Desktop\\QA\\Chromedriver\\chromedriver_win32 (3)\\chromedriver.exe");

        //navigate to Bing
        WebDriver webDriver = new ChromeDriver();
        webDriver.navigate().to("https://www.bing.com");
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //enter text in search box and click on it
        WebElement item = webDriver.findElement(By.name("q"));
        item.sendKeys("Telerik Academy Alpha");
        item.submit();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //validate title of first result
        WebElement searchResult = webDriver.findElement(By.xpath("//*[@id='b_results']/li[1]/div[1]/h2"));

        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        Assert.assertEquals(searchResult.getText(), "IT Career Start in 6 Months - Telerik Academy Alpha",
                "Search result doesn't match.");

        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.close();

    }

}

